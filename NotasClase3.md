# Clase 3 - 16/05/2022

En la clase de hoy vamos a ver conceptos de la sintaxis básica del lenguaje **SQL (Structure Query Language)**. Se puede ejecutar consultas, insertar, modificar y eliminar registro. Crear base de datos, tablas, procedimientos y vistas. Establecer permisos especiales para tablas, procedimientos y vistas.

Para realizar una consulta del estilo **select** se corre la siguiente sentencia:

```sql
SELECT campos FROM table_name;
```

Si se utiliza el * se llama a todos los campos que hay en esa tabla.

Si se quiere agregar una filtro se debe utilizar la sentencia **WHERE**:

```sql
SELECT campos FROM table_name WHERE campos>=0;
```

Los operadores de logicos y de comparación en SQL son:

![alt text](oplogicos.png "Operadores Logicos en SQL")


Para seleccionar información que sea diferente:

```sql
SELECT DISTINCT id_sytem_user FROM system_user;
```