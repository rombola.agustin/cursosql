SELECT id_game FROM game 
UNION 
SELECT name FROM game;
SELECT * FROM game WHERE name LIKE '%Ultimate%';
SELECT * FROM game WHERE name LIKE '%Ult_mate%';
SELECT * FROM game WHERE name LIKE '[A-H]%';

/* SUB CONSULTAS */
/* Trae el usuario que tenga el id_user_type maximo en la tabla user_type*/
SELECT id_system_user, last_name 
FROM system_user
WHERE id_user_type = (SELECT max(id_user_type) FROM user_type);

SELECT id_system_user
FROM vote
WHERE value = (SELECT FLOOR (AVG(value)) FROM vote); /* FLOOR para convertir a entero */