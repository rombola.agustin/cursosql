# Clase 5 - 23/05/2022

## Consultas y subconsultas

## Clausula UNION

Se utiliza para hacer uniones, es decir unificar consultas de tipo **SELECT**. Debe ser la misma consulta y debe ser el mismo tipo de datos. (La cantidad de columnas debe ser la misma en cada tabla en caso de que sean tablas diferentes)

```sql
SELECT id_game FROM game 
UNION
SELECT completed FROM play;
```

## Tipos de Datos

- Número entero -> int
- Texto -> text(n)
- Alfanumérico -> varchar(n)
- Fecha -> date
- Fecha y Hora -> datetime
- True or False -> Boolean
- Decimal -> decimal(p,s) 
- Numérico -> numeric(p,s)

## Operador Like

Sirve para hacer consultas especiales. Es decir si usas el % podes indicar que no importa si antes o después no te importa que hay.

```sql
SELECT * FROM game WHERE name LIKE '%Ultimate%';
```

El comodin *_* se puede utilizar para desconocer el conocimiento de un sólo caracter.

```sql
SELECT * FROM game WHERE name LIKE '%Ult_mate%';
```

Uso de chorchetes en el LIKE:

```sql
SELECT * FROM game WHERE name LIKE '[A-B]%';
```

## Subconsultas

Reglas para utilizarlas:

- Debe ir entre paréntesis y debe tetener una sola columna o expresión
- No se puede utilizar LIKE o BETWEEN
- No se puede utilizar ORDER BY

Un ejemplo:

```sql
/* Trae el usuario que tenga el id_user_type maximo en la tabla user_type*/
FROM system_user
WHERE id_user_type = (SELECT max(id_user_type) FROM user_type);
```

Serian los id_system_user que votaron por encima del promedio.

```sql
SELECT id_system_user
FROM vote
WHERE value = (SELECT FLOOR (AVG(value)) FROM vote); /* FLOOR para convertir a entero */
```