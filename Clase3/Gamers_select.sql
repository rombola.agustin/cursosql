USE gamers;
SELECT * FROM system_user; 

/* PARA DEVOLVER LOS CAMPOS DIFERENTES: */
SELECT DISTINCT first_name FROM system_user;

SELECT * FROM system_user WHERE id_system_user = 56;
SELECT * FROM system_user WHERE first_name = 'Gillie';
SELECT * FROM system_user WHERE id_system_user BETWEEN 10 AND 15; /* incluye el valor inicial y final */
