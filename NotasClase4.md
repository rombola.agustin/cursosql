# Clase 4 - 18/05/2022

Reconocer e implementar setencias complementarias de SQL y las funciones que brinda el lenguaje SQL.

## Sublenguajes de SQL

Para ver las tablas disponibles en la DB:

```sql
SHOW TABLES;
```

Un ejemplo tipico de query usando **SELECT, FROM Y WHERE** en SQL:

```sql
SELECT commentary, id_system_user FROM commentary WHERE id_game = '73';
```

Hay una forma de buscar en strings utilizando **IN**. El **IN** funciona si el string es el MISMO. Es decir si el string en la tabla es "Hola Roberto" y se busca solamente "Hola" no encontrará nada.


```sql
SELECT id_game, name FROM game WHERE name IN('Riders Republic','The Dark Pictures: House Of Ashes');
```

Ahora si para buscar un pedazo de string se puede usar **REGEXP** o se puede usar **LIKE** y *%*. También para evitar el problema de mínisculas y mayúsculas la función *upper()* o *lower()* para solucionar.

```sql
SELECT id_game, name FROM game WHERE name REGEXP('Gran');
SELECT id_game, name FROM game WHERE name LIKE '%Riders%';
SELECT id_game, name FROM game WHERE name LIKE '%Riders%';
SELECT id_game, name FROM game WHERE LOWER(name) LIKE '%riders%';
SELECT id_game, name FROM game WHERE UPPER(name) LIKE '%RIDERS%';
```

## Sentencia ORDER BY y sentencia LIMIT

Para obtener la tabla ordenada se puede usar **ORDER BY** y aclarar si se quiere en orden ascendente ASC o descendente DESC.

```sql
SELECT columns FROM table_name WHERE condicion ORDER BY columna tipo_de_orden;
SELECT id_game, name FROM game ORDER BY name ASC; /* Ordenad por nombre de la A a la Z */
```

La sentencia **LIMIT** limita la cantidad de datos.

```sql
SELECT id_game, name FROM game LIMIT 5,10;
```

Se pueden combinar y ver los ultimos 5 por ejemplo.

```sql
SELECT id_game, name FROM game ORDER BY name DESC LIMIT 5;
```

En SQL Server se utiliza **TOP** en lugar se usar **LIMIT**.

## Uso de ALIAS

Se le puede poner un **ALIAS** a una columna para usarla con mayor facilidad dentro de mi código. Lógicamente esto no modifica el nombre REAL de la columna en la tabla.

```sql
SELECT
	su.id_system_user AS id,
    su.password AS pass
FROM system_user AS su /* donde su es la abreviacion*/
ORDER BY su.id_system_user;
```

## Funciones de Agregación o Agrupación

Se usa el **GROUP BY** y **AS**. Existe la funcion **COUNT()** para contar la cantidad, **MIN()** para buscar el mínimo, el **MAX()** para buscar el máximo. El **AS** lo uso para ponerle un nombre razonable y conocido de ante mano a esa columna. 

```sql
SELECT COUNT(*) AS total_level FROM level_game;
SELECT MIN(id_level) AS min_level FROM level_game;
SELECT MAX(id_level) AS max_level FROM level_game;
```

La función **SUM()** para sumar los valores de las columnas y **AVG()** se puede usar para sacar el promedio.

```sql
SELECT value, id_game FROM vote WHERE id_game = 1;
SELECT SUM(value) AS suma_value FROM vote WHERE id_game = 1;
SELECT AVG(value) AS promedio_value FROM vote WHERE id_game = 1;
```

Se pueden hacer combinacionas más sofisticadas aprovechando el **GROUP BY**. En este ejemplo, agrupo por usuario la cantidad de juegos.

```sql
SELECT * FROM play;
SELECT id_system_user AS user, COUNT(*) AS game_by_user FROM play GROUP BY id_system_user;
```

## Sentencia HAVING

Es la misma idea que el *WHERE* pero para evaluar resultados de las funciones que se utilizan. El ejemplo a continuación utiliza el **HAVING** para evaluar el resultaod de la función **COUNT()**.

```sql
SELECT id_system_user AS user, COUNT(*) AS game_by_user 
FROM play 
GROUP BY id_system_user
HAVING COUNT(*)>1;
```

## Sentencia JOIN

Permite combinar registros diferentes tablas. Se utiliza las *keys* que tengan en común. Hay distintos tipos de **JOIN**:

- INNER JOIN: Trae todas las filas de la tabla que se indique. Con el **ON** se indica cual es la condición a cumplir para relacionar las tablas, osea el campo que coincide en ambas tablas. En **FROM** se indica la tabla principal y con **INNER JOIN** se indica la tabla secundaria.

```sql
SELECT * FROM play;
SELECT * FROM game;
SELECT p.id_system_user AS user, g.id_game AS game, g.name, g.id_level AS level
FROM play AS p 
INNER JOIN game AS g 
ON(p.id_game = g.id_game);
```

- LEFT JOIN AND RIGHT JOIN: Trae todos los registros de la primer tabla (Izquierda) y trae la info de la segunda

```sql
SELECT p.id_system_user AS user, g.id_game AS game, g.name, g.id_level AS level
FROM game AS g
LEFT JOIN play as p
ON(p.id_game = g.id_game);
```

Si se hiciera **RIGHT JOIN** pero exactamente al revés se obtendría el mismo resultado.

```sql
SELECT p.id_system_user AS user, g.id_game AS game, g.name, g.id_level AS level
FROM play AS p
RIGHT JOIN game as g
ON(p.id_game = g.id_game);
```

- FULL JOIN: Retorna todas las filas de la tabla derecha y todas las de la tabla derecha. Es como combinar LEFT JOIN y RIGHT JOIN.

*FULL JOIN no es soportado por MYSQL. Pero otros motores de SQL si soportan esta funcionalidad, por ejemplo Microsoft SQL Server*

