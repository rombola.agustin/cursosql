USE gamers;
SELECT id_game, name FROM game ORDER BY name ASC; /* Ordenad por nombre de la A a la Z */
SELECT id_game, name FROM game LIMIT 5,10;
SELECT id_game, name FROM game ORDER BY name DESC LIMIT 5;

/*Uso de ALIAS*/
SELECT
	su.id_system_user AS id,
    su.password AS pass
FROM system_user AS su /* donde su es la abreviacion*/
ORDER BY su.id_system_user;

SELECT COUNT(*) AS total_level FROM level_game;
SELECT MIN(id_level) AS min_level FROM level_game;
SELECT MAX(id_level) AS max_level FROM level_game;

SELECT value, id_game FROM vote WHERE id_game = 1;
SELECT SUM(value) AS suma_value FROM vote WHERE id_game = 1;
SELECT AVG(value) AS promedio_value FROM vote WHERE id_game = 1;

SELECT * FROM play;
SELECT id_system_user AS user, COUNT(*) AS game_by_user FROM play GROUP BY id_system_user;

/* Usando HAVING */

SELECT id_system_user AS user, COUNT(*) AS game_by_user 
FROM play 
GROUP BY id_system_user
HAVING COUNT(*)>1;

/* Sentencia JOIN*/
/*1) INNER JOIN*/
SELECT * FROM play;
SELECT * FROM game;
SELECT p.id_system_user AS user, g.id_game AS game, g.name, g.id_level AS level
FROM play AS p 
INNER JOIN game AS g 
ON(p.id_game = g.id_game);
/* 2) LEFT JOIN AND RIGHT JOIN */
SELECT p.id_system_user AS user, g.id_game AS game, g.name, g.id_level AS level
FROM game AS g
LEFT JOIN play as p
ON(p.id_game = g.id_game);

SELECT p.id_system_user AS user, g.id_game AS game, g.name, g.id_level AS level
FROM play AS p
RIGHT JOIN game as g
ON(p.id_game = g.id_game);

