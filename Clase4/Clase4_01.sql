USE gamers;
/* Explorando la base de datos */
SHOW TABLES; /* Para ver las tablas */
SELECT * FROM game limit 10; /* limit para ver algunos pocos datos */
SELECT * FROM commentary limit 10;

/* 1) Todos los comentarios sobre juegos desde 2019 en adelante */
SELECT commentary, comment_date FROM commentary WHERE comment_date > '2018-12-31';

/* 2) Todos los comentarios sobre juegos anteriores a 2011 */
SELECT commentary, comment_date FROM commentary WHERE comment_date <= '2011-01-01';

/* 3) Usuarios y texto de comentarios sobre juegos con id_game sea 73 */
SELECT commentary, id_system_user FROM commentary WHERE id_game = 73;

/* 4) Los usuarios y texto de comentarios sobre juegos que NO tienen id_game = 73 */
SELECT commentary, id_system_user FROM commentary WHERE id_game != 73; /* También se puede <> para el distinto */
SELECT commentary, id_system_user FROM commentary WHERE NOT id_game = 73; /* Se puede usar NOT también */

/* 5) Aquellos juegos que son nivel 1*/
SELECT name, id_game  FROM game WHERE id_level = 1;

/* 6) Todos los juegos de nivel 14 o mas */
SELECT id_game, name FROM game WHERE id_level >= 14;

/* 7) Aquellos juegos de nombre 'Riders Republic'o 'The Dark Pictures: House Of Ashes' */
SELECT id_game, name FROM game WHERE name IN('Riders Republic','The Dark Pictures: House Of Ashes');
SELECT id_game, name FROM game WHERE name = 'Riders Republic' OR name = 'The Dark Pictures: House Of Ashes';
/* Ambas soluciones son analogas pero el IN es más eficiente y sofisticado */

/*8) Aquellos juegos que su nombre empiece con 'Gran' */
SELECT id_game, name FROM game WHERE name REGEXP('Gran');
SELECT id_game, name FROM game WHERE name LIKE '%Riders%';
SELECT id_game, name FROM game WHERE LOWER(name) LIKE '%riders%';
SELECT id_game, name FROM game WHERE UPPER(name) LIKE '%RIDERS%';
