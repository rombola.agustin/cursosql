/*Trae los plays agrupados por id_system_user contando el id_game*/
USE gamers;
SELECT COUNT(id_game) AS cantidad, id_system_user 
FROM play 
GROUP BY id_system_user;

/* SUBLENGUAGE DDL */
/* Uso se la sentencia CREATE */

CREATE TABLE friend(
	id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    first_name VARCHAR(30),
    last_name VARCHAR(30),
    email VARCHAR(50)
);

/* ALTER */

ALTER TABLE friend
ADD age INT;

ALTER TABLE friend
MODIFY email VARCHAR(80) NOT NULL;

SELECT CONCAT(first_name, ' ', last_name) AS complete_name
FROM system_user;

SELECT UCASE(first_name) AS fist_name_upper
FROM system_user;

SELECT REVERSE(first_name)
FROM system_user;

SELECT SUBSTRING("SQL Tutorial", 5, 3) AS ExtractString; /* (start at position 5, extract 3 characters): */

SELECT CURDATE() AS today;
