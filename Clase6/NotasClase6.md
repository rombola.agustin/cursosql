# Clase 6 - 25/05/2022


Uso del **GROUP BY**:

```sql
/*Trae los plays agrupados por id_system_user contando el id_game*/
SELECT COUNT(id_game) AS cantidad, id_system_user 
FROM play 
GROUP BY id_system_user;
```

## Sublenguaje DDL (Data Definition Language)

En principio, SQL esta compuesto por 5 sublenguajes:

- DDL: Data Definition Language
- TCL Transaction Control Language
- DML: Data Manipulation Language
- DCL: Data Control Language
- DQL: Data Query Language

Las sentencias de DDL permiten crear, modificar, alterar y eliminar objetos de la DB. Las mismas son:

- :star: **CREATE** se puede crear objetos en la base de datos.

```sql
CREATE TABLE table_name ([parametros]);
```

Para definir las columnas o los campos, se debe indicar el nombre y el tipo de dato contiene (con o sin límite).

```sql
CREATE TABLE pay (
	id pay INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
	amount REAL NOT NULL,
	currency VARCHAR(20) NOT NULL,
	date_pay DATE NOT NULL,
	pay_type VARCHAR(50), /* Este campo si podria no ser llenado */
	id_system_user INT NOT NULL,
	id_game INT NOT NULL
);
```

**AUTO_INCREMENT** significa que automática se llena y se va llenando. **NOT NULL** significa que no puede ser nulo, es decir este campo es de llenado obligatorio. 

La ventaja de usar **VARCHAR(n_characters)** optimiza el uso de memoria. Ya que guarda como máximo *n* caracateres pero si llegan a ser menos de *n* guarda solamente esos.

Si agrego **PRIMARY KEY** eso significa que este campo va a ser el identificador primario de esa tabla. (No es obligatorio indicar una PRIMARY KEY a la hora de crear una tabla).

Para clonar una tabla se utiliza también la sentencia **CREATE**:

```sql
CREATE TABLE friend_backup LIKE friend;
```

- :star: **ALTER** permite modificar el schema de una tabla. Se puede agregar campos o se puede modificar mediante **ADD** o **MODIFY** respectivamente.

```sql
ALTER TABLE friend
ADD age INT;

ALTER TABLE friend
MODIFY email VARCHAR(80) NOT NULL;
```

- :star: **DROP** elimina directamente la tabla. Por lo general, no es deseable eliminar la tabla. Sobre todo porque al borrar una tabla se puede alterar las relaciones que esa tabla tenga con las demás tablas que no es deseable eliminar.


```sql
DROP TABLE friend;
```
- :star: **TRUNCATE** sirve para vaciar una tabla. Es decir eliminar los datos de la tabla sin borrar la tabla en si. Nuevamente, hay que tener especial cuidado con esta sentencia ya que puede haber datos relaciones con datos de otra tabla.

```sql
TRUNCATE TABLE friend;
```

## Funciones Escalares

Existen 2 tipos de funciones escalares: *integradas, almacenadas*.

### Funciones integradas

- **CONCAT()**: fuciona cadena de caracteres. Es decir concatena dos campos:

```sql
SELECT CONCAT(first_name, ' ', last_name) AS complete_name /* Le agrego un espacio*/
FROM system_user;
```

- **UCASE(), LCASE()**: convierte en mayúscula y minúscula.

```sql
SELECT UCASE(first_name) AS fist_name_upper
FROM system_user;
```

- **REVERSE()**: lo devuelve revertido. Es decir de derecha a izquierda.

```sql
SELECT REVERSE(first_name)
FROM system_user;
```

- **TRIM()**: elimina los espacios adelante y atrás, es decir en los extremos de un texto. (**LTRIM()** hace lo mismo pero con left y **RTRIM()** lo mismo pero con right).

- **SPACE()**: devuelve los espacios que tiene un bloque. Es decir no la cantidad, si no un string con la cantidad de spaces que tenga el string que se selecciono.

- **CHAR_LENGTH()** cuenta la cantidad de carácteres en un bloque de texto.

- **SUBSTRING()** extrae una determinada cantidad de carácteres de un string. Requiere que se le indique las posición de donde empezar y cuantos carácteres se desean sustraer:

```sql
SELECT SUBSTRING("SQL Tutorial", 5, 3) AS ExtractString; /* (start at position 5, extract 3 characters): */
```

Existen varias funciones más y se pueden ver en: <https://www.w3schools.com/mysql/mysql_ref_functions.asp>

### Funciones de fechas

- **CURDATE()** devuelve la fecha actual y **CURTIME()** devuelve la hora actual.

```sql
SELECT CURDATE() AS today;

SELECT CURTIME() AS hour_now;
```